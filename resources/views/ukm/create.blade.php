@extends('layouts.layout')

@section('content')
<div class="col-12">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Kelola UKM</h4>
      <P>Tambah & Update Data</P>
      <form class="form-sample">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-form-label">Nama UKM</label>
              <input class="form-control" placeholder="Masukkan nama UKM" />
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-form-label">Jadwal Pertemuan UKM</label>
              <input class="form-control" placeholder="Masukkan nama UKM" />
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-form-label">Bidang UKM</label>
              <select class="form-control" name="bidang-ukm" id="bidang-ukm">
                <option value="Penalaran">Penalaran</option>
                <option value="Olahraga">Olahraga</option>
                <option value="Seni Budaya">Seni Budaya</option>
                <option value="Agama">Agama</option>
                <option value="Bela Negara">Bela Negara</option>
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-form-label">Logo UKM</label>
              <input type="file" class="form-control" placeholder="Masukkan nama UKM" />
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label class="col-form-label">Deskripsi UKM</label>
              <textarea class="form-control" placeholder="Masukkan nama UKM"></textarea>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-form-label">Media Sosial</label>
              <input class="form-control" placeholder="Masukkan media sosial UKM" />
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-form-label">Contact Person</label>
              <input class="form-control" placeholder="Masukkan contact person UKM" />
            </div>
          </div>
          <hr>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-form-label">List Kegiatan</label>
              <input class="form-control" placeholder="Masukkan list kegiatan UKM" />
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-form-label">List Prestasi</label>
              <input class="form-control" placeholder="Masukkan nama UKM" />
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-form-label">Kegiatan</label>
              <table>
                <tr>
                  <td class="col-6">1. Belajar</td>
                  <td><input type="button" class="form-control bg-danger text-white" value="hapus"></td>
                </tr>
              </table>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-form-label">Prestasi</label>
              <table>
                <tr>
                  <td class="col-6">1. Belajar</td>
                  <td><input type="button" class="form-control bg-danger text-white" value="hapus"></td>
                </tr>
              </table>
            </div>
          </div>
        </div>
        <button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
        <a href="/dashboard" class="btn btn-light">Cancel</a>
      </form>
    </div>
  </div>
</div>
@endsection